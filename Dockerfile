FROM node:latest

RUN mkdir parse

ADD . /parse
WORKDIR /parse
RUN npm install

EXPOSE 1337

RUN mkdir certificate
RUN mkdir cloud
#ADD $CERTIFICATE_URI /certificate
RUN wget $CLOUD_CODE_FILE_URL
RUN wget $CERTIFICATE_FILE_URL
# ADD $CLOUD_CODE_FILE /cloud/main.js
RUN mv $CLOUD_CODE_NAME cloud/
RUN mv $CERTIFICATE_NAME certificate/
RUN export NODE_PATH=/parse/node_modules
# Uncomment if you want to access cloud code outside of your container
# A main.js file must be present, if not Parse will not start

# VOLUME /parse/cloud
CMD npm start
